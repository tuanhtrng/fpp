#ifndef COMPLEX_HPP
#define COMPLEX_HPP

#include <cstdint>

#include "types/f77.hpp"
#include "types/real.hpp"

namespace fpp {

// 8 bytes of storage
using COMPLEX8 = fpp::f77::COMPLEX;

// 16 bytes of storage
namespace {
    struct fortran_double_complex_type {
        fpp::f77::DOUBLE_PRECISION dr;
        fpp::f77::DOUBLE_PRECISION di;
    };
} using DOUBLE_COMPLEX = fortran_double_complex_type;

// 16 bytes of storage
using COMPLEX16 = fpp::DOUBLE_COMPLEX;

// 32 bytes of storage
namespace {
    struct fortran_complex32_type {
        fpp::REAL16 dr;
        fpp::REAL16 di;
    };
} using COMPLEX32 = fortran_complex32_type;

}

#endif//ndef COMPLEX_HPP
