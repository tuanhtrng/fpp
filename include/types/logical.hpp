#ifndef LOGICAL_HPP
#define LOGICAL_HPP

#include "types/f77.hpp"
#include "types/byte.hpp"
#include "types/integer.hpp"

namespace fpp {

// 1 byte of storage
using LOGICAL1 = fpp::BYTE;

// 2 byte of storage
using LOGICAL2 = fpp::INTEGER2;

// 4 byte of storage
using LOGICAL4 = fpp::f77::LOGICAL;

// 8 byte of storage
using LOGICAL4 = fpp::INTEGER4;

}

#endif//ndef LOGICAL_HPP
