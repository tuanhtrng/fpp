#ifndef REAL_HPP
#define REAL_HPP

#include "types/f77.hpp"

namespace fpp {

// 4 bytes of storage
using REAL4 = fpp::f77::REAL;

// 8 bytes of storage
using REAL8 = fpp::f77::DOUBLE_PRECISION;

// 16 bytes of storage
using REAL16 = long double;

}

#endif//ndef REAL_HPP
