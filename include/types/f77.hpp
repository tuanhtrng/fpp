#ifndef F77_HPP
#define F77_HPP

#include <cstdint>

namespace fpp { namespace f77 {

// 4 bytes of storage
using INTEGER = std::int32_t;

// 4 bytes of storage
using REAL = float;

// 8 bytes of storage
using DOUBLE_PRECISION = double;

// 8 bytes of storage
namespace {
    struct fortran_complex_type {
        fpp::f77::REAL r;
        fpp::f77::REAL i;
    };
} using COMPLEX = fortran_complex_type;


// 4 bytes of storage
using LOGICAL = std::int32_t;

// 1 byte of storage
using CHARACTER = std::uint8_t;

}}

#endif//ndef F77_HPP
