#ifndef INTEGER_HPP
#define INTEGER_HPP

#include <cstdint>

#include "types/f77.hpp"

namespace fpp {

// 2 bytes of storage
using INTEGER2 = std::int16_t;

// 4 bytes of storage
using INTEGER4 = fpp::f77::INTEGER;

// 8 bytes of storage
using INTEGER8 = std::int64_t;

}

#endif//ndef INTEGER_HPP
