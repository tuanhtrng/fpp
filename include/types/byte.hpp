#ifndef BYTE_HPP
#define BYTE_HPP

#include <cstdint>

namespace fpp {

// 1 byte of storage
using BYTE = std::int8_t;

}

#endif//ndef BYTE_HPP
