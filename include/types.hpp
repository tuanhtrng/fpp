#ifndef TYPES_HPP
#define TYPES_HPP

#include "types/f77.hpp"
#include "types/byte.hpp"
#include "types/integer.hpp"
#include "types/real.hpp"
#include "types/complex.hpp"
#include "types/logical.hpp"

#endif//ndef TYPES_HPP
